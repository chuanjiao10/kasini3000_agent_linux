#

if grep -Eq 'Jammy Jellyfish' /etc/issue || grep -Eq 'Jammy Jellyfish' /etc/*-release
then
  echo 'linux is ubuntu2204'
  if [ -h /usr/bin/pwsh ]
  then
    echo 'powershell installed on ubuntu2204'
    if grep -Eq 'powershell' /etc/ssh/sshd_config
    then
      echo 'powershell_sshd_good'
      exit 0
    else
      echo '' >>  /etc/ssh/sshd_config
      echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
      echo 'UseDNS no' >>  /etc/ssh/sshd_config
      echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
      systemctl restart sshd.service
      echo 'powershell_sshd_fixed'
      exit 0
    fi
  else
    echo 'installing powershell now'
    sudo snap remove powershell
    sudo curl -o /tmp/a.deb https://packages.microsoft.com/config/ubuntu/22.04/packages-microsoft-prod.deb
    sudo dpkg -i /tmp/a.deb
    sudo apt update
    sudo add-apt-repository universe
    sudo apt remove -y powershell
    sudo apt install -y gss-ntlmssp libgssapi-krb5-2
    sudo apt-get install -y powershell
    if [ $? -ne 0 ]
    then
      echo 'install fall'
      exit 3
    else
      echo 'install powershell sucess'
    fi

    if grep -Eq 'powershell' /etc/ssh/sshd_config
    then
      echo 'powershell_sshd_good'
      exit 0
    else
      echo '' >>  /etc/ssh/sshd_config
      echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
      echo 'UseDNS no' >>  /etc/ssh/sshd_config
      echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
      systemctl restart sshd.service
      echo 'powershell_sshd_fixed'
      exit 0
    fi
  fi
else
  echo 'linux not ubuntu2204'
  exit 2
fi




