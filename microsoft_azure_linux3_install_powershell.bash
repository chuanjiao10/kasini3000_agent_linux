#

if rpm -q centos-release  | grep -Eq 'el6'
then
  echo 'not support centos6'
  exit 1
fi


if grep -Eq 'Microsoft Azure Linux 3' /etc/issue || grep -Eq 'Microsoft Azure Linux 3' /etc/*-release
then
  echo 'Microsoft Azure Linux 3'
  if [ -h /usr/bin/pwsh ]
  then
    echo 'powershell installed Microsoft Azure Linux 3'
    if grep -Eq 'powershell' /etc/ssh/sshd_config
    then
      echo 'powershell_sshd_good'
      exit 0
    else
      echo '' >>  /etc/ssh/sshd_config
      echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
      echo 'UseDNS no' >>  /etc/ssh/sshd_config
      systemctl restart sshd.service
      echo 'powershell_sshd_fixed'
      exit 0
    fi
  else
    echo 'installing powershell now'
    dnf remove -y powershel
    dnf install -y powershell
    #dnf install -y https://packages.microsoft.com/cbl-mariner/2.0/prod/Microsoft/x86_64/Packages/p/powershell-7.4.6-1.cm.x86_64.rpm
    dnf install https://packages.microsoft.com/azurelinux/3.0/prod/ms-oss/x86_64/Packages/p/powershell-7.4.6-1.cm.x86_64.rpm
    if [ $? -ne 0 ]
    then
      echo 'install fall'
      exit 3
    else
      echo 'install powershell sucess'
      if grep -Eq 'powershell' /etc/ssh/sshd_config
      then
        echo 'powershell_sshd_good'
        exit 0
      else
        sed -i '/PermitRootLogin/d' /etc/ssh/sshd_config
        sleep 1
        echo '' >>  /etc/ssh/sshd_config
        echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
        echo 'UseDNS no' >>  /etc/ssh/sshd_config
        echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
        systemctl restart sshd.service
        echo 'powershell_sshd_fixed'
        exit 0
      fi
    fi
  fi
else
  echo 'linux not Microsoft Azure Linux 3'
  exit 2
fi
