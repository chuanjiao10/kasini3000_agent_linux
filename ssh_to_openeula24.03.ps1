﻿#从win，linux中。用ssh客户端，经ssh协议，直接连接被控机中的powershell，而不需要经过bash。
#需要在被控机上安装powershell，并修改sshd_config。
#支持【win终端】，支持经session复制文件，传递对象。
param
(
	[String]$目的ip地址 = '192.168.111.222',
	$端口 = 22,
	$账户 = 'root',
	$ssh_key_文件路径 = "$env:USERPROFILE\.ssh\id_ed25519"
)

$msg = @'
需要ssh.exe,低版本win请去这里下载：https://github.com/PowerShell/Win32-OpenSSH/releases
建议在Windows Terminal中使用：
"commandline" : "C:\\Program Files\\PowerShell\\7-preview\\pwsh.exe -noexit -file d:\\ps脚本\\centos7.ps1",
'@

Write-Warning $msg

if ($IsLinux -eq $true)
{
    $ssh_key_文件路径 = "/root/.ssh/id_rsa"
}

if ( ($PSVersionTable.psversion.major -ge 7) -and ($PSVersionTable.psversion.minor -ge 2) )
{
}
else
{
	Write-Error '需要powershell v7.2 版本，及以上。'
}

Write-Host -ForegroundColor green "进入目的ip【$目的ip地址】"
Enter-PSSession -Session (New-PSSession -ConnectingTimeout 3000 -HostName $目的ip地址 -Port $端口 -UserName root -KeyFilePath $ssh_key_文件路径)
if ($LASTEXITCODE -eq 0)
{
}
else
{
    Write-Error '错误：被控机需要安装powershell，并修改sshd_config文件。'
    write-host 'https://gitee.com/chuanjiao10/kasini3000_agent_linux' -ForegroundColor Yellow
}

