﻿#

if ($IsLinux -eq $True)
{
}
else
{
	Write-Error '错误：不是linux。返回码1'
	exit 1
}

$输入文件全路径 = '/etc/ssh/sshd_config'
$找什么 = '^PermitRootLogin'
$替换成啥 = 'PermitRootLogin without-password'

if (Test-Path -LiteralPath $输入文件全路径)
{
}
else
{
	Write-Error '错误：找不到输入文件，返回码2'
	exit 2
}

$youn = Select-String -LiteralPath $输入文件全路径 -Pattern '^PermitRootLogin without-password' -Quiet
if ($youn -eq $true)
{
	Write-Error '错误：root密码登录已经禁用。返回码3'
	exit 3
}

$Values2 = Get-Content -LiteralPath $输入文件全路径 -Raw -Encoding utf8

if ( ($crlf变lf -eq $true) -and ($cr变lf -eq $true) )
{
	Write-Error '错误：输入错误，返回码111'
	exit 111
}

if ($crlf变lf -eq $true)
{
	$Values3 = $Values2.Replace("`r`n",'乂').split('乂')
	$已经工作 = $true
}

if ($cr变lf -eq $true)
{
	$Values3 = $Values2.split("`r")
	$已经工作 = $true
}

if ($Values3)
{
}
else
{
	$Values3 = $Values2.split("`n")
}

if ($去掉井开头的行 -eq $true)
{
	[System.Collections.ArrayList]$动态数组 = @()
	foreach ($i in $Values3)
	{
		$i2 = $i.Split('#')[0]

		if ($i2 -eq '')
		{
			continue
		}

		if ($i2 -eq $i)
		{
			$null = $动态数组.Add($i)
			continue
		}

		$i2.Replace("`t",'').Replace(' ','')
		if ($i2.Length -gt 0)
		{
			$null = $动态数组.Add($i)
		}
	}

	$Values11 = $动态数组.ToArray()
	$已经工作 = $true
}
else
{
	$Values11 = $Values3
}


if ( ($找什么 -eq $null) -or ($找什么 -eq '') )
{
}
else
{
	$you = Select-String -LiteralPath $输入文件全路径 -Pattern $找什么 -Quiet
	if ($you -eq $true)
	{
		for ($i11 = 0; $i11 -lt $Values11.Length ;$i11++)
		{
			if ($Values11[$i11] -match $找什么)
			{
				$Values11[$i11] = ''
			}
		}
	}

	$Values11 += $替换成啥
	$已经工作 = $true
}

if ($已经工作 -eq $true)
{
	$日期 = Get-Date -Format 'yyyy_MM_dd_HH_mm_ss'
	Copy-Item -LiteralPath '/etc/ssh/sshd_config' -Destination $('/etc/ssh/sshd_config' + '_' + $日期)
	$Values12 = $Values11 -join "`n"
	$utf8无bom编码对象 = New-Object System.Text.UTF8Encoding($False)
	[system.IO.File]::WriteAllText($输入文件全路径, $Values12, $utf8无bom编码对象)
	Start-Sleep -Seconds 1
	systemctl daemon-reload
	systemctl restart sshd.service
}
Write-Host -ForegroundColor green '信息：脚本正常完成，返回码0'
exit 0
