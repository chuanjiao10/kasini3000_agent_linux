#
if [ -h /usr/bin/pwsh ]
then
  echo 'powershell installed on rhel9'
  if grep -Eq 'powershell' /etc/ssh/sshd_config
  then
    echo 'powershell_sshd_good'
    exit 0
  else
    echo '' >>  /etc/ssh/sshd_config
    echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
    echo 'UseDNS no' >>  /etc/ssh/sshd_config
    echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
    systemctl restart sshd.service
    echo 'powershell_sshd_fixed'
    exit 0
  fi
else
  echo 'installing powershell now'
  curl -o /etc/yum.repos.d/microsoft.repo  https://packages.microsoft.com/config/rhel/9/prod.repo
  dnf remove -y powershel
  dnf install -y powershell
  if [ $? -ne 0 ]
  then
    echo 'install fall'
    exit 3
  else
    echo 'install powershell sucess'
    if grep -Eq 'powershell' /etc/ssh/sshd_config
    then
      echo 'powershell_sshd_good'
      exit 0
    else
      echo '' >>  /etc/ssh/sshd_config
      echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
      echo 'UseDNS no' >>  /etc/ssh/sshd_config
      echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
      systemctl restart sshd.service
      echo 'powershell_sshd_fixed'
      exit 0
    fi
  fi
fi





