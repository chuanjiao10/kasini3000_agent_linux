#

if grep -Eq 'bookworm' /etc/issue || grep -Eq 'bookworm' /etc/*-release
then
  echo 'linux is debian12'
  if [ -h /usr/bin/pwsh ]
  then
    echo 'powershell installed on debian12'
    if grep -Eq 'powershell' /etc/ssh/sshd_config
    then
      echo 'powershell_sshd_good'
      exit 0
    else
      echo '' >>  /etc/ssh/sshd_config
      echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
      echo 'UseDNS no' >>  /etc/ssh/sshd_config
      echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
      systemctl restart sshd.service
      echo 'powershell_sshd_fixed'
      exit 0
    fi
  else
    echo 'installing powershell now'
    apt update
    apt install -y curl gnupg apt-transport-https sudo
    curl -s https://packages.microsoft.com/keys/microsoft.asc | sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/microsoft.gpg --import
    chmod 644 /etc/apt/trusted.gpg.d/microsoft.gpg
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-bookworm-prod bookworm main" > /etc/apt/sources.list.d/microsoft.list
    #echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-bullseye-prod bullseye main" > /etc/apt/sources.list.d/microsoft.list
    sudo apt update
    sudo apt remove -y powershell
    sudo apt install -y powershell
    if [ $? -ne 0 ]
    then
      echo 'install fall'
      exit 3
    else
      echo 'install powershell sucess'
    fi

    if grep -Eq 'powershell' /etc/ssh/sshd_config
    then
      echo 'powershell_sshd_good'
      exit 0
    else
      echo '' >>  /etc/ssh/sshd_config
      echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
      echo 'UseDNS no' >>  /etc/ssh/sshd_config
      echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
      systemctl restart sshd.service
      echo 'powershell_sshd_fixed'
      exit 0
    fi
  fi
else
  echo 'linux not debian12'
  exit 2
fi




