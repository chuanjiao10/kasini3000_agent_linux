﻿
# powershell is 【open source】，【free】，【cross-platform】，【green (copy or decompression then run) this better than python】software.

# powershell是开源，免费，跨平台，绿色（复制后，或解压后，即可运行的。这一点比python好）软件。

# kasini3000_agent_linux

bash script.one click install powershell on linux, offline install powershell on linux, modify sshd_config for PsRemote.

One-click one key install powershell linux 一键 安装 debian ubuntu centos alpine

SaltStack是python开发的，c/s架构的批量运维工具。它的被控机agent，在2020年5月份被发现了，远程执行命令漏洞。详见cve-2020-11651 cve-2020-11652。

《卡死你3000》和ansible永远没有类似的漏洞！《卡死你3000》中，开端口监听的agent是： linux的open-sshd

对于kasini3000的linux被控机，不启动进程，不监听端口。sshd认证过你的ssh-key-file后才会拉起pwsh。linux被控机、linux堡垒机，安全无忧。

kasini3000不用密码登录，只用rsa4096的key登录。独创的双key-file登录机制，让你可以随时更新-更换一个key。

MIT LICENSE

# All script tested.Welcome give me star !

原则上，只会增加，至少维护期为3年的，lts版，服务器版linux，的一键安装脚本。

应该有一定的用户群，最好不是衍生版linux。

卡死你3000 官方技术支持群 qq群：700816263


------

# powershell 安装包 镜像网址

问：如何高速下载Powershell的二进制文件？

答： 感谢【powershell鼓吹士】分享

上海交通大学镜像站缓存了github powershell的release

https://mirrors.sjtug.sjtu.edu.cn/github/Powershell


------

# 问：如何升级powershell？
答：

yum： yum update powershell

dnf：dnf update powershell

apt：apt install powershell

arm，alpine ：需要去github下载新版的tar包，重新解压。基本上就是删除旧的，下载，解压，新的。

yum安装特定版本：

yum install -y powershell-7.2.23

apt安装特定版本：

1 用apt-cache madison powershell列出。

2 apt install powershell=7.2.23-1.deb

------

# 离线安装powershell （是指从win主控机，到linux被控机，通过复制文件目录，安装powershell）

以达到脱离互联网安装，不受外国限制的目的。

问：如何离线安装linux版powershell？

答：https://gitee.com/chuanjiao10/kasini3000/wikis/%E5%AE%89%E8%A3%85/%E7%A6%BB%E7%BA%BF%E5%AE%89%E8%A3%85

问：离线安装linux版powershell有何缺点？

答：

linux被控机内的powershell，没有安装微软源（/etc/yum.repos.d/microsoft.repo），和证书。无法通过yum update，dnf update来升级powershell。
只能通过在主控机，解压并存放新版本的安装文件，重新离线安装，来升级。

一般来讲，需要在主控机，每年左右升级1次powershell。
被控机powershell7.2已经很稳定了，基本上可以2年升级一次。所以说这个缺点并不会让你难受。

问：离线安装linux版powershell有何好处？

答：
不分发行版。不用rpm包，或deb包。因为从主控机，到被控机，直接复制的是：解压出来的目录文件。

```powershell
dnf -y install libicu
curl -L https://github.com/PowerShell/PowerShell/releases/download/v7.4.7/powershell-7.4.7-linux-x64.tar.gz -o /tmp/powershell.tar.gz
mkdir -p /opt/microsoft/powershell/7
tar zxf /tmp/powershell.tar.gz -C /opt/microsoft/powershell/7
chmod +x /opt/microsoft/powershell/7/pwsh
ln -s /opt/microsoft/powershell/7/pwsh /usr/bin/pwsh
echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
echo 'UseDNS no' >>  /etc/ssh/sshd_config
#echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
systemctl restart sshd.service
```



------

问：卡死你3000，依赖哪些powershell版本？
答：

powershell版本需求：

您应该使用，超越稳定版本的powershell，以便减少卡死你3000，产生错误的几率。

1 powershell v6的最新版本，即6.2.7     从2023年1月1日起，卡死你3000不再支持powershell v6.x

2 powershell v7.0的最新版本，即7.0.12 不支持。从2024年1月1日起，不再支持。

3 powershell v7.1的最新版本，即7.1.7	不支持。从2024年1月1日起，不再支持。

4 🔥powershell v7.2的最新版本，即7.2.23	支持，并建议使用。从2022年5月1日起，或powershellv7.2.6起，卡死你3000开始支持powershell v7.2

5 powershell v7.3的最新版本，即7.3.11	不支持。

6 🔥powershell v7.4的最新版本，即7.4.6	支持，并建议使用。

7 powershell v7.5.x  不支持。

8 powershell preview最新版。不建议使用，欢迎高手测试并向官方群反馈bug。


测试时间:2024-11-23-----2025-11-23

注意：powershell v7.3 不再支持在win7，win2008r2被控机上运行。


------

# 不支持 centos6
# 支持 centos7 ,kernel=3.10
# 支持 centos8（2021-12-31 eol。不建议使用,建议用衍生版）
# 支持 rhel8 ,kernel=4.18

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/centos7_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# AlmaLinux release 9 。AlmaLinux----翻译成【阿乐马linux】，或【饿了吗linux】如何？
# Rocky Linux release 9
# Oracle Linux 9
# RHEL9 kernel=5.14

在alma-linux9，rocky-linux9，oracle-linux9，rhel9，上安装powershell v7.4.x ：

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/rhel9_install_powershell.bash | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# AlmaLinux release 10
# Rocky Linux release 10
# Oracle Linux 10
# RHEL10 kernel=6.11

在alma-linux10，rocky-linux10，oracle-linux10，rhel10，上安装powershell v7.4.x ：

~~curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/rhel10_install_powershell.bash | /usr/bin/bash~~

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# Amazon Linux release 2 (Karoo) 。对标centos7。

官网：https://aws.amazon.com/cn/amazon-linux-2/

下载：https://cdn.amazonlinux.com/os-images/latest/

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/amazon_linux2_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# Amazon Linux 2023 ,kernel=6.1

到目前为止，aws linux已经发布了两个主要版本，一个在2010年，另一个在2017年。

但是，对于第三个版本（目前叫做aws linux 2023），将承诺两年的发布周期，每个版本的支持期为五年。

aws linux 2023已经发布preview，基于fedora35（openssl3，kernel 6.1）围观：

https://aws.amazon.com/cn/linux/amazon-linux-2023

尴尬了，ubuntu虽然好，也属于美国派系。但和centos7差异太大。

而centos9 stream，本身就是fedora 的nightly build.

暂时没有做一键安装脚本。可以参考使用rhel9 install powershell的脚本。

可以通过离线安装方法，从主控机一键安装。

------

# 麒麟v10高级服务器版x86-64 (Tercel)。对标centos8。kernel=4.19

麒麟v10官网：https://www.kylinos.cn/  暂不开放给个人用户试用

银河麒麟高级服务器操作系统V10 SP3,是一款 基于 openEuler 构建的“国产Linux发行版”。


支持arm版：需要用tar方式，安装arm版powershell。

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/kylin_linux_advanced_server_v10_tercel_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# 统信UOS服务器版2020 x86_64(kongzi) 。对标centos8。kernel=4.19 <---推荐

统信官网，下载地址： https://www.chinauos.com/resource/download-server

未激活时，需要禁用这两个repo：
/etc/yum.repos.d/UOS-AppStream.repo
/etc/yum.repos.d/UOS-Base.repo

支持arm版：需要用tar方式，安装arm版powershell。

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/chinaous_enterprise_server_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# 阿里云“龙蜥” Anolis OS 8，对标centos8。kernel=4.19

下载地址： 阿里云“龙蜥”linux官网，下载地址： https://openanolis.cn/download

支持x86版，若有arm版也支持。

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/anolisos8_install_powershell.bash  | /usr/bin/bash

# 阿里云“龙蜥” Anolis OS 23，kernel=6.6 <---推荐

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/anolisos23_install_powershell.bash | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# Alibaba Cloud Linux 2，对标centos7。kernel=4.19
# Alibaba Cloud Linux 3，对标centos8。kernel=5.10

下载地址：

http://mirrors.aliyun.com/alinux/3/image/

http://mirrors.aliyun.com/alinux/2/image/

支持x86版，若有arm版也支持。Alibaba Cloud Linux 2，Alibaba Cloud Linux 3，这两个版本，用同一个脚本安装。

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/alibaba_cloud_linux_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# 腾讯OpenCloudOS，对标centos8。kernel=5.4

下载地址：

http://www.opencloudos.org/?page_id=505


curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/tencent_opencloudos8.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# Rocky Linux release 8，对标centos8。 <---推荐，但必须使用国内yum源，防止安装包内投毒

下载地址：https://mirror.rockylinux.org/mirrormanager/mirrors

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/rocky_linux_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# AlmaLinux release 8，对标centos8。 kernel=4.18  <---推荐，但必须使用国内yum源，防止安装包内投毒

下载地址：https://mirrors.almalinux.org/isos.html

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/almalinux_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# oracle linux server 8，对标centos8。uke kernel=5.4 ,rhck kernel=4.18

下载地址：https://yum.oracle.com/oracle-linux-isos.html

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/oraclelinux_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

Linux Kernel 5.8，当时Linux内核贡献排行统计显示：

华为在代码修改行（line changed）位列第一，占比 27.8%，是第二名的3倍以上；
代码贡献（changesets）位列第二名，占比 8.6%，仅次于Intel。

![kernel](kernel6.png)

# 华为openEuler 20.03 LTS。 kernel=4.19  <--- 推荐

# openEuler 22.03 LTS。 kernel=5.10  <--- 推荐优先使用。

# openEuler 24.03 LTS。 kernel=6.6  <--- 推荐 rpm包兼容rhel9

只支持lts版。但不是lts版，应该也能用脚本安装powershell。

下载地址：https://www.openeuler.org/zh/download/

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/openeuler_install_powershell.bash  | /usr/bin/bash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

发行版推出3年内由团队维护，3---5年由lts团队维护。共5年。

# debian8 （收费支持）

```
sudo apt-get update
sudo apt-get install -y curl apt-transport-https

# Import the public repository GPG keys
curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -

# Register the Microsoft Product feed
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-debian-jessie-prod jessie main" > /etc/apt/sources.list.d/microsoft.list'

# Update the list of products
sudo apt-get update

# Install PowerShell
sudo apt-get install -y powershell
```

# debian9 （不建议使用）kernel=4.9

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/debian9_install_powershell.bash  | /usr/bin/bash

# debian10 kernel=4.19

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/debian10_install_powershell.bash  | /usr/bin/bash

# debian11 kernel=5.10

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/debian11_install_powershell.bash | /usr/bin/bash

# debian12 kernel=6.1 (release day 2023-06-17 )

2024-10-24 powershell 7.4.6 发布时，psteam正式支持。

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/debian12_install_powershell.bash | /usr/bin/bash

<!--
下述临时安装方法：
```
apt install libicu72
curl  https://github.com/PowerShell/PowerShell/releases/download/v7.4.5/powershell_7.4.5-1.deb_amd64.deb
dpkg -i ./powershell_7.4.5-1.deb_amd64.deb
echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
echo 'UseDNS no' >>  /etc/ssh/sshd_config
echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
systemctl restart ssh.service
```
-->


debian官方下载地址： https://cdimage.debian.org/cdimage/release/

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

<!--
sudo dpkg -i https://github.com/PowerShell/PowerShell/releases/download/v7.2.1/powershell_7.2.1-1.deb_amd64.deb
-->

------

# ubuntu 14.04 （收费支持）

```
sudo apt-get install libunwind8 libicu52
sudo dpkg -i https://packages.microsoft.com/ubuntu/14.04/prod/pool/main/p/powershell/powershell_6.2.0-1.ubuntu.14.04_amd64.deb
```

# ubuntu 16.04 （不建议使用）

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/ubuntu1604_install_powershell.bash  | /bin/bash


# ubuntu 18.04 lts kernel=4.15 （不建议使用）

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/ubuntu1804_install_powershell.bash  | /bin/bash


# ubuntu 20.04 lts kernel=5.4

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/ubuntu2004_install_powershell_2.bash  | /bin/bash


# ubuntu 22.04 lts kernel=5.15 <---推荐，但必须使用国内apt源，防止安装包内投毒

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/ubuntu2204_install_powershell.bash  | /bin/bash

# ubuntu 24.04 lts kernel=6.8

2024-10-24 powershell 7.4.6 发布时，psteam正式支持。
<!--
```shell
wget https://mirror.sjtu.edu.cn/ubuntu/pool/main/i/icu/libicu72_72.1-3ubuntu3_amd64.deb
wget https://github.com/PowerShell/PowerShell/releases/download/v7.4.5/powershell_7.4.5-1.deb_amd64.deb

dpkg -i ./libicu72_72.1-3ubuntu3_amd64.deb
dpkg -i ./powershell_7.4.5-1.deb_amd64.deb
echo 'Subsystem powershell /usr/bin/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
echo 'UseDNS no' >>  /etc/ssh/sshd_config
echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
systemctl restart ssh.service
```

=====

还要过1---2个月，powershell才能支持ubuntu 24.04 lts

-->
curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/ubuntu2404_install_powershell.bash  | /bin/bash

ubuntu lts官方下载地址： http://releases.ubuntu.com/

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# alpine linux  3.8, 3.9 ,3.10 ,3.11 ,3.12

1 disable dropbear sshd,then enable openssh-server sshd.

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/alpine_del_dropbear_install_opensshserver.ash  | /bin/ash

2 install powershell and edit sshd_config.

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/alpine_install_powershell.ash  | /bin/ash

## alpine 3.13

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/alpine3.13_install_powershell.ash  | /bin/ash

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

树莓派，win10-iot，arm 详见：

https://blog.ironmansoftware.com/daily-powershell/powershell-remoting-ssh-raspberry-pi/

https://github.com/PowerShell/PowerShell/releases


------

# MicroSoft azurelinux 2.0 kernel=5.15

官网：https://github.com/microsoft/azurelinux

下载： https://aka.ms/mariner-2.0-x86_64-iso

安装powershell：

curl -L https://gitee.com/chuanjiao10/kasini3000_agent_linux/raw/master/microsoft_cbl_mariner_install_powershell.bash  | /bin/bash

rpm包，dnf安装源。

## 注意：

此脚本将开启linux被控机的，ssh的， root + 密码登录。这是为了自动推送ssh公钥到'/root/.ssh/authorized_keys'。
在首次运行krun时，krun调用 [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] ，将从[ nodelist.csv ] 读取linux node的 ip,port,root,pwd 。
为了linux被控机安全，建议推送ssh公钥后，手动运行下列命令，关闭root+密码登录。

script will enable root account and password login on linux node by ssh. "root" account and himself password,is used to push ssh pub key to '/root/.ssh/authorized_keys'. [ winscp复制主控机公钥到被控机_win2linux_pwd.ps1 ] will read linux node ip,port,root,pwd in [ nodelist.csv ] when krun first run.

For the security of the linux node, it is recommended to manually run the following command after pushing the SSH public key to close the root + password login.
to disable root + password login,you must run 【zkj_gb关闭linux被控机root密码登录z.ps1 -nodeip 1.1.1.1】 at the kasini3000 master.

or

```powershell
pwsh -f /your_path/ssh_root_passwd_login_on.ps1

pwsh -f /your_path/ssh_root_passwd_login_off.ps1
```

------

# suse Enterprise(sles) or opensuse, install powershell:

https://en.opensuse.org/PowerShell

```
sudo zypper update && \
  sudo zypper install libicu60_2 libopenssl1_0_0
sudo zypper install \
  --allow-unsigned-rpm \
  https://s3.jcloud.sjtu.edu.cn/899a892efef34b1b944a19981040f55b-oss01/github-release/PowerShell/PowerShell/releases/download/v7.4.5/powershell-7.4.5-1.rh.x86_64.rpm
```

------

# 2025-03-05，kasini3000 被控机（remote），新增支持 freebsd13，freebsd 14，freebsd 15

2025-03-01,powershell v7.5.x（基于 .net 9） 正式进入freebsd官方“ports”软件源。

https://github.com/freebsd/freebsd-ports/tree/main/shells/powershell

信息：

https://www.freshports.org/shells/powershell/

### 安装：

```powershell
mkdir -p /usr/local/etc/pkg/repos
echo 'FreeBSD: { enabled: no }

ustc:{
    url: "http://mirrors.ustc.edu.cn/freebsd-pkg/${ABI}/latest",
    mirror_type: "none",
    signature_type: "none",
    fingerprints: "/usr/share/keys/pkg",
    enabled: yes
} '  > /usr/local/etc/pkg/repos/FreeBSD.conf
pkg update -f
pkg install -y powershell
echo '' >>  /etc/ssh/sshd_config
echo 'Subsystem powershell /usr/local/lib/powershell/pwsh -sshs -NoLogo -NoProfile' >>  /etc/ssh/sshd_config
echo 'UseDNS no' >>  /etc/ssh/sshd_config
echo 'PermitRootLogin yes' >>  /etc/ssh/sshd_config
service sshd restart

```

### 3 离线安装：

依赖库：
brotli: 1.1.0,1
dotnet-host: 9.0.2
gettext-runtime: 0.23.1
icu: 76.1,1
indexinfo: 0.3.1_1
krb5: 1.21.3
libinotify: 20240724
libpowershell-native: 7.4.0
libunwind: 20240221_2
powershell: 7.5.0_1
readline: 8.2.13_2
terminfo-db: 20231209


------


# linux 发行版 eol 日期整理！

Amazon Linux release 2 (Karoo) 2023-06-30

**centos7** 2024-06-30

	下列centos7衍生版，和centos7的eol日期相同：

	Alibaba Cloud Linux 2，

**centos8** 已经eol

**红帽 rhel 8.4**  2023 年 5 月 30 日结束。

https://access.redhat.com/zh_CN/support/policy/updates/errata#RHEL8_Life_Cycle

	    * 阿里云“龙蜥” Anolis OS 8

	    * Rocky Linux release 8

	    * AlmaLinux release 8

	    * oracle linux server 8

fedora server 35 目测发行后1年01月

https://docs.fedoraproject.org/en-US/releases/eol/

麒麟v10高级服务器版x86-64 (Tercel)  eol日期 = 未知

统信UOS服务器版2020 x86_64(kongzi)  eol日期 = 未知 。目测和debian10相同

华为openEuler 20.03 LTS（SP2）eol日期 = 发行4年内。每个sp版本循环滚动增加4年。

https://www.openeuler.org/zh/other/lifecycle/

debian9 已经eol

debian10 2022-08

debian11 2026-08 。发行版推出3年内由团队维护，3---5年由lts团队维护。共5年。

ubuntu 16.04 lts 已经eol

ubuntu 18.04 lts 2023-04

ubuntu 20.04 lts 2025-04

ubuntu 22.04 lts 2027-04


------

# note:

For non-server Linux systems, I have NO plans to make a one-click installation script.

Please go to the official website of powershell to find the installation tutorial.

https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-linux?view=powershell-7.2

